import React from 'react';
import PropTypes from "prop-types";

const Input = ({
   label,
   placeholder,
   type,
   onChange,
   name,
   error,
   errorText,
   value,
   }) => (
   <div className="form-group">
     <div className="input_container">
       <label
          className={`pull-left label-input ${error ? 'label-input-has-error ' : '' }`}>
          {label}
       </label>
       <input
         name={name}
         type={type}
         value={value}
         onChange={onChange}
         placeholder={placeholder}
         className={`input ${error ? 'input-has-error ' : '' }`}/>
         {error && <span className="error"> {errorText} </span> }
       <p className="input_img">
        *
        <span className="tooltiptext">required</span>
       </p>
     </div>
    </div>
);


Input.propTypes = {
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    errorText: PropTypes.string,
    error: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
};

export default Input;