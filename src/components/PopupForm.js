import React from 'react';
import PropTypes from 'prop-types';

const PopupForm = ({
 children,
 header,
 avatar
}) => (
    <div className="login">
        <div className="login-header">
            <div className="avatar">
                <img alt="Avatar" src={avatar}/>
            </div>
            <div className="login-label">
                {header}
            </div>
        </div>
        <div className="input-fields">
            {children}
        </div>
    </div>
);

PopupForm.propTypes = {
    children: PropTypes.element.isRequired,
    header: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
};

export default PopupForm;

