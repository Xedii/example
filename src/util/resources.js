import axios from 'axios';

import constants from '../constants';
// @see https://github.com/mzabriskie/axios#creating-an-instance
export const dataResource = axios.create({
  baseURL: constants.baseURL
});
