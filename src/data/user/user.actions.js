import bows from 'bows';

import {
    SIGN_IN_SUCCESS,
    SIGN_IN_ERROR,
    SIGN_IN_START
} from '../types';

import { dataResource } from '../../util/resources';

const log = bows('user.actions');

const signInStart = () => ({ type: SIGN_IN_START });
const signInSuccess = (payload) => ({ payload, type: SIGN_IN_SUCCESS });
const signInError = (error) => ({ error, type: SIGN_IN_ERROR });

let actions = {
    signIn(email, password) {
        log('signIn');
        return (dispatch) => {
            const startAction = signInStart();
            log('startAction', startAction);
            dispatch(startAction);
            const query = {
                email,
                password
            };
            dataResource.post('/api/login', query)
                .then(result => {
                    log('success', result);
                    const successAction = signInSuccess(result.data);
                    dataResource.defaults.headers.common['Authorization'] = result.data.token;
                    log('successAction', successAction);
                    console.log('data');
                    dispatch(successAction);
                })
                .catch(error => {
                    log('error', error);
                    const errorAction = signInError(error);
                    dispatch(errorAction);
                    log('errorAction', errorAction);
                })
        }
    }
};

export default actions;
