import {
    SIGN_IN_SUCCESS,
    SIGN_IN_ERROR,
} from '../types';

const INITIAL_STATE = {
    token: null,
};

export default (state = INITIAL_STATE, action) => {
    let newState;
    switch (action.type) {
        case SIGN_IN_SUCCESS:
            newState = Object.assign({}, state);
            newState.token = action.payload.token;
            return newState;
        case SIGN_IN_ERROR:
            newState = Object.assign({}, state);
            newState.error = 'Authentication Failed.';

            return newState;
        default:
            return state;
    }
};
