import React, { PureComponent } from 'react';

class AuthenticationLayoutContainer extends PureComponent {
    render() {
        return (
            <div className="container">
                <div className="fullscreen_bg">

                <form className="form-signin">
                    {this.props.children}
                </form>
                </div>
            </div>
        );
    }
}
export default AuthenticationLayoutContainer;
