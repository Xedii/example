import { bindActionCreators }   from 'redux';
import { connect } from 'react-redux';

import userActions              from '../../data/user/user.actions';
import AuthenticationContainer  from './AuthenticationContainer';

const mapStateToProps = ({ user }) => {
    return { user };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userActions: bindActionCreators(userActions, dispatch),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthenticationContainer);
