import { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import bows from 'bows';

const log = bows('AuthenticationContainer');

class AuthenticationContainer extends Component {
    state = {
        isAuthenticated: false
    };

    static propTypes = {
        children: PropTypes.element,
        user: PropTypes.object,
    };

    static contextTypes = {
      store: PropTypes.object.isRequired
    };

    async componentWillMount() {
        try {
            if (!this.props.user.token) browserHistory.replace('/signin');
            this.setState({ isAuthenticated: true });
        }
        catch (err) {
            log(`[ERROR] ${err.message}`)
        }
    }

    render() {
        const { isAuthenticated } = this.state;
        if (isAuthenticated) return this.props.children;
        return null;
    }
}
export default AuthenticationContainer;
