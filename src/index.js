import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import store from './store'
import MainRouter from './modules/MainRouter';
import 'react-bootstrap';
import './assets/styles/main.scss';

render(
    <Provider store={store}>
        <MainRouter />
    </Provider>,
    document.querySelector('#root')
);