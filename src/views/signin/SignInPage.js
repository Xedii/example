import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SignInForm from './components/SignInForm';

export default class SignInPage extends Component {
    state = {
        errors: {
            email: false,
            password: false
        },
        user: {
            email: '',
            password: '',
            remember: false
        },
        loading: false
    };

    static propTypes = {
        userActions: PropTypes.object.isRequired
    };

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValidation = re.test(String(email));
        this.setState({
            errors:{
                email: !isValidation
            }
        })
    };

    onChange = (event) => {
        const field = event.target.name;
        const user = this.state.user;
        const isEmail = field === 'email';

        user[field] = event.target.value;

        if(isEmail) this.validateEmail(event.target.value);

        this.setState({ user });
    };

    onSubmit = async (event) => {
        // prevent default action. in this case, action is the form submission event
        event.preventDefault();
        //login action
    };

    render() {
        return (
            <SignInForm
                onSubmit={this.onSubmit}
                onChange={this.onChange}
                errors={this.state.errors}
                user={this.state.user}
            />
        );
    }
}
