import { bindActionCreators }   from 'redux';
import { connect }              from 'react-redux';

import userActions              from '../../data/user/user.actions';
import SignInPage               from './SignInPage';

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignInPage);
