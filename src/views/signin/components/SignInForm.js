import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Form } from 'react-bootstrap';

import Input from '../../../components/Input';
import PopupForm from '../../../components/PopupForm'

import Avatar from '../../../assets/images/avatar.png'

const SignInForm = ({
    onSubmit,
    onChange,
    errors,
    user,
}) => (
    <PopupForm
        avatar={Avatar}
        header={"LOGIN"}
    >
        <Form onSubmit={onSubmit}>
            <Input
                type={"email"}
                label={"EMAIL ADDRESS"}
                placeholder={"Enter your email address"}
                name={'email'}
                onChange={onChange}
                value={user.email}
                error={errors.email}
                errorText={"Invalid email address"}
            >
            </Input>
            <Input
                type={"password"}
                label={"PASSWORD"}
                placeholder={"Enter your password"}
                name={'password'}
                onChange={onChange}
                value={user.password}
            />
            <div className="checkbox">
                <input type="checkbox"
                       id="remember_me"
                       className="remember-me"
                       name="remember_me"
                       defaultChecked={user.remember}
                       onChange={onChange}
                       value={user.remember}
                />
                <label >
                    <div className="text"> REMEMBER ME </div>
                </label>
            </div>
            <div className="submit-container">
                <Link style={{ textDecoration: 'none' }} to={'/reset-password'}> <p> Forgotten password?  </p> </Link>
                <button type="submit" value="Submit" className="login-button">LOGIN</button>
            </div>
        </Form>
    </PopupForm>
);

SignInForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default SignInForm;
