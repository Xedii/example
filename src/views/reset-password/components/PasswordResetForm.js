import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

import Input from '../../../components/Input';
import PopupForm from '../../../components/PopupForm'

import Avatar from '../../../assets/images/avatar.png'

const ResetPasswordForm = ({
    onSubmit,
    onChange,
    errors,
    user,
}) => (
    <PopupForm
        avatar={Avatar}
        header={"RESET PASSWORD"}
    >
        <form onSubmit={onSubmit}>

            <p className="reset-password-text">
                Hey, it happens to everyone.<br/>
                Just lets us know what email address you use to login<br/>
                and we'll send you an email with instructions.<br/>
            </p>
            <Input
                type={"email"}
                label={"EMAIL ADDRESS"}
                placeholder={"Enter your email address"}
                name={'email'}
                onChange={onChange}
                value={user.email}
                error={errors.email}
                errorText={"Invalid email address"}
            />
            <div className="reset-submit-container">
                <Link to={'/signin'}>
                    <button type="submit" className="cancel-button">CANCEL</button>
                </Link>
            <button type="submit" value="Submit" className="login-button reset-password-button">RESET PASSWORD</button>
            </div>
        </form>
    </PopupForm>
);

ResetPasswordForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
};

export default ResetPasswordForm;
