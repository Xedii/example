import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PasswordResetForm from './components/PasswordResetForm';

export default class SignInPage extends Component {
    // set the initial component state
    state = {
        errors: {},
        user: {
            email: '',
        }
    };

    static propTypes = {
        userActions: PropTypes.object.isRequired
    };

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValidation = re.test(String(email));
        this.setState({
            errors:{
                email: !isValidation
            }
        })
    };

    onChange = (event) => {
        const field = event.target.name;
        const user = this.state.user;
        const isEmail = field === 'email';

        user[field] = event.target.value;

        if(isEmail) this.validateEmail(event.target.value);

        this.setState({ user });
    };

    onSubmit = async (event) => {
        // prevent default action. in this case, action is the form submission event
        event.preventDefault();
        //reset password action
    };

    render() {
        return (
            <PasswordResetForm
                onSubmit={this.onSubmit}
                onChange={this.onChange}
                errors={this.state.errors}
                user={this.state.user}
            />
        );
    }
}
