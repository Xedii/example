import { bindActionCreators }   from 'redux';
import { connect }              from 'react-redux';

import userActions              from '../../data/user/user.actions';
import SignUpPage               from './ResetPassword';

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignUpPage);
