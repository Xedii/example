import React from 'react';
import { Router, Route }  from 'react-router';

import { history } from '../store';

// Views
import signIn from '../views/signin';
import resetPassword from '../views/reset-password';
import home from '../views/home';

// Containers
import AuthenticationLayoutContainer from '../containers/AuthenticationLayoutContainer';
import AuthenticationContainer from '../containers/AuthenticationContainer';

const MainRouter = () => (
    <Router history={history}>
        <Route component={AuthenticationContainer}>
            <Route path="/" component={home} />
        </Route>

        <Route component={AuthenticationLayoutContainer}>
            <Route exact path="/signin" component={signIn} />
            <Route exact path="/reset-password" component={resetPassword} />
        </Route>
    </Router>
);

export default MainRouter;